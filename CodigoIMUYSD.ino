//Universidad del Valle de Guatemala
//Diseño e innovacion
//Esteban Alejrandro Avalos Morales
//Carnet: 15059


// Este programa realiza la funcion de obtener datos del sensor (IMU Gy502), mediante comunicacion I2C con estes.
// Se realizo la normalizacion de los datos de los angulos roll y pitch, mediante los valores de Gx y Gy
// Tambien se obtienen los valores de las aceleraciones en los 3 ejes del prototipo
// estos valores se guardaron en una memora micro Sd, mediante la libreria SD de arduino. Se utilizo un adaptador 
// micro Sd para arduino. Los valores se guardaron en un archivo .Csv, separando pr columanas los valores obtenidos
// de angulos y aceleraciones 

// Codigo de toma de datos de la IMU basado en el codigo obtenido en el siguiente Link
//https://naylampmechatronics.com/blog/45_Tutorial-MPU6050-Aceler%C3%B3metro-y-Giroscopio.html


// se incluyen las librerias  a usar
#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"
#include <SD.h>


// se  abre un archivo tipo file
File myFile;


// se utilizo la direccion de la IMU
MPU6050 sensor;

// Declaracion de variables a utilizar
int ax, ay, az;
int gx, gy, gz;
int i = 0;
long tiempo_prev;
float dt;
float ang_x, ang_y;
float ang_x_prev, ang_y_prev;
bool Estado = false;



void setup() {
  
  Serial.begin(9600);    //Iniciando puerto serial
  Wire.begin();           //Iniciando I2C
  sensor.initialize();    //Iniciando el sensor
  
  Serial.print("Iniciando SD ...");
  if (!SD.begin(4)) {
    Serial.println("No se pudo inicializar");
    return;
  }
  Serial.println("inicializacion exitosa");

  if (sensor.testConnection()) Serial.println("Sensor iniciado correctamente");
  else Serial.println("Error al iniciar el sensor");
}

void loop() {
  // Leer las aceleraciones y velocidades angulares
  myFile = SD.open("datos.csv", FILE_WRITE);//abrimos  el archivo
  if (myFile) {
    
  sensor.getAcceleration(&ax, &ay, &az);
  sensor.getRotation(&gx, &gy, &gz);

  dt = (millis() - tiempo_prev) / 1000.0;
  tiempo_prev = millis();

  //Calcular los ángulos con acelerometro
  float accel_ang_x = atan(ay / sqrt(pow(ax, 2) + pow(az, 2))) * (180.0 / 3.14);
  float accel_ang_y = atan(-ax / sqrt(pow(ay, 2) + pow(az, 2))) * (180.0 / 3.14);

  //Calcular angulo de rotación con giroscopio y filtro complemento
  ang_x = 0.98 * (ang_x_prev + (gx/131) * dt) + 0.02 * accel_ang_x;
  ang_y = 0.98 * (ang_y_prev + (gy/131) * dt) + 0.02 * accel_ang_y;

// conversion  a Aceleraciones
  ang_x_prev = ang_x;
  ang_y_prev = ang_y;

  // Guardar valores en archivo 
  Serial.print("Escribiendo SD: ");
  myFile.print(ax);
  myFile.print(",");
  myFile.print(ay);
  myFile.print(",");
  myFile.print(az);
  myFile.print(",");
  myFile.print(ang_x);
  myFile.print(",");
  myFile.println(ang_y);
  myFile.close(); //cerramos el archivo
  


}
 else {
    Serial.println("Error al abrir el archivo");
  }
  
  delay(100);

}
